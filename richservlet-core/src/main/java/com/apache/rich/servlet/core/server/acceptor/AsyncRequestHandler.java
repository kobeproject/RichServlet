package com.apache.rich.servlet.core.server.acceptor;


import com.apache.rich.servlet.core.server.acceptor.AsyncRequestReceiver;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * 
 * @author wanghailing
 *
 */
public  class AsyncRequestHandler extends AsyncRequestReceiver {

    
    public static AsyncRequestHandler build() {
        return new AsyncRequestHandler();
    }
    public AsyncRequestHandler() {
    			super();
    }
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest msg)
			throws Exception {
		
	}

    
}
