package com.apache.rich.servlet.core.server.rest;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.HttpResponseStatus;

import java.util.List;
import java.util.concurrent.Callable;

import com.apache.rich.servlet.core.server.rest.response.RichServletServerHttpResponseBuilder;
import com.apache.rich.servlet.core.server.rest.response.HttpResponseResult;
import com.apache.rich.servlet.core.server.rest.response.RichServletServerHttpResponse;

import com.apache.rich.servlet.core.server.rest.interceptor.Interceptor;
import com.apache.rich.servlet.core.server.rest.controller.URLController;
import com.apache.rich.servlet.common.utils.SerializeUtils;
import io.netty.handler.codec.http.DefaultHttpResponse;

/**
 * 
 * @author wanghailing
 *
 */
public class ExecutorTask implements Callable<RichServletServerHttpResponse> {

    // httpContext used for controller invoking
    private final HttpContext httpContext;
    // related controller
    private final URLController handler;
    // interceptor list
    private final List<Interceptor> interceptor;

    public ExecutorTask(HttpContext httpContext, List<Interceptor> interceptor, URLController handler) {
        this.httpContext = httpContext;
        this.interceptor = interceptor;
        this.handler = handler;
    }

    @Override
    public RichServletServerHttpResponse call() {
        // call interceptor chain of filter
		RichServletServerHttpResponse httpResponse=new RichServletServerHttpResponse();

        for (Interceptor every : interceptor) {
            if (!every.filter(httpContext)){
            		httpResponse.setHttpResponse(RichServletServerHttpResponseBuilder.create(httpContext, HttpResponseStatus.FORBIDDEN));  // httpcode 403
            		return httpResponse;
            }
        }

        // call controller method
        HttpResponseResult result = handler.call(httpContext);

        DefaultHttpResponse response;

        switch (result.getHttpStatus()) {
        case SUCCESS:
            if (result.getHttpContent() != null) {
                ByteBuf content = Unpooled.wrappedBuffer(SerializeUtils.encode(result.getHttpContent()));
                response = RichServletServerHttpResponseBuilder.create(httpContext, content); // httpcode 200
               // httpResponse.setHttpContent(RichServletServerHttpResponseBuilder.create(content));
            } else{
                response = RichServletServerHttpResponseBuilder.create(httpContext, HttpResponseStatus.NO_CONTENT);          // httpcode 204
            }	
            break;
        case RESPONSE_NOT_VALID:
            response = RichServletServerHttpResponseBuilder.create(httpContext, HttpResponseStatus.BAD_GATEWAY);            // httpcode 502
            break;
        case PARAMS_CONVERT_ERROR:
        case PARAMS_NOT_MATCHED:
            response = RichServletServerHttpResponseBuilder.create(httpContext, HttpResponseStatus.BAD_REQUEST);             // httpcode 400
            break;
        case SYSTEM_ERROR:
            response = RichServletServerHttpResponseBuilder.create(httpContext, HttpResponseStatus.INTERNAL_SERVER_ERROR);    // httpcode 500
            break;
        default:
            response = RichServletServerHttpResponseBuilder.create(httpContext, HttpResponseStatus.INTERNAL_SERVER_ERROR);    // httpcode 500
            break;
        }
        httpResponse.setHttpResponse(response);
        // call interceptor chain of *handler*. returned DefaultFullHttpResponse
        // will be replaced to original instance
        for (Interceptor every : interceptor) {
        		RichServletServerHttpResponse newResponse = every.handler(httpContext, httpResponse);
            if (newResponse != null){
            		httpResponse = newResponse;
            }
        }

        return httpResponse;
    }

}
