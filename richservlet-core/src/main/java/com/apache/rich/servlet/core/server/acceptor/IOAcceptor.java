/**
 * 
 */
package com.apache.rich.servlet.core.server.acceptor;

/**
 * @author wanghailing
 *
 */
public interface IOAcceptor {
	
	void eventLoop();

    void join() throws InterruptedException;

    void shutdown();
}
