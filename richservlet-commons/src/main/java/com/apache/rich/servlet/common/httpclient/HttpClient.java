package com.apache.rich.servlet.common.httpclient;

import com.apache.rich.servlet.common.httpclient.HttpRequest;
import com.apache.rich.servlet.common.httpclient.HttpResponse;

public interface HttpClient {
	HttpResponse postJson(HttpRequest arg0);

	HttpResponse post(HttpRequest arg0);

	HttpResponse get(HttpRequest arg0);

	HttpResponse getDowload(HttpRequest arg0);

	HttpResponse postDowload(HttpRequest arg0);

	HttpResponse upload(HttpRequest arg0);
}