package com.apache.rich.servlet.common.exception;

public class HServerException extends Exception {
	private static final long serialVersionUID = 5414954847382611427L;
	private String errorCode;
	private String errorMessage;

	public HServerException(String errorCode, String errorMessage) {
		super(errorMessage);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return this.errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}