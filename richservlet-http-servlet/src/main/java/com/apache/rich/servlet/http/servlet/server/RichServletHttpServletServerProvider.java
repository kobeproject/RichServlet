/**
 * 
 */
package com.apache.rich.servlet.http.servlet.server;

import com.apache.rich.servlet.http.servlet.realize.RichServletHttpServlet;

import com.apache.rich.servlet.http.servlet.server.acceptor.RichServletHttpServletServerAsyncAcceptor;

/**
 * @author wanghailing
 *
 */
public class RichServletHttpServletServerProvider{

	private static class LazyHolder {
		private static final RichServletHttpServletServerProvider INSTANCE = new RichServletHttpServletServerProvider();
	}

	private RichServletHttpServletServerProvider() {

	}

	public static final RichServletHttpServletServerProvider getInstance() {
		return LazyHolder.INSTANCE;
	}

	
	public RichServletHttpServletServer service(RichServletHttpServlet richServletHttpServlet) {
			RichServletHttpServletServer httpServer = new RichServletHttpServletServer();
            httpServer.ioAcceptor(new RichServletHttpServletServerAsyncAcceptor(httpServer,richServletHttpServlet));
            return httpServer;
	}

}
