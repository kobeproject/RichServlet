
package com.apache.rich.servlet.http.servlet.realize.configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;


import com.apache.rich.servlet.http.servlet.realize.adapter.RichServletHttpComponentConfigurationAdapter;
/**
 * http servlet Configuration
 * @author wanghailing
 *
 */
public class RichServletHttpServletConfiguration extends RichServletHttpComponentConfigurationAdapter<HttpServlet, RichServletHttpServletConfig> {

    public RichServletHttpServletConfiguration(Class<? extends HttpServlet> servletClazz,
                                String... urlPatterns) {
        super(servletClazz, urlPatterns);
    }

    public RichServletHttpServletConfiguration(Class<? extends HttpServlet> componentClazz) {
        super(componentClazz);
    }

    public RichServletHttpServletConfiguration(HttpServlet component, String... urlPatterns) {
        super(component, urlPatterns);
    }

    public RichServletHttpServletConfiguration(HttpServlet servlet) {
        super(servlet);
    }

    @Override
    protected void doInit() throws ServletException {
        this.component.init(this.config);
    }

    @Override
    protected void doDestroy() throws ServletException {
        this.component.destroy();
    }

    @Override
    protected RichServletHttpServletConfig newConfigInstance(
            Class<? extends HttpServlet> componentClazz) {
        return new RichServletHttpServletConfig(this.component.getClass().getName());
    }

    public RichServletHttpServletConfiguration addInitParameter(String name, String value) {
        super.addConfigInitParameter(name, value);
        return this;
    }
}
