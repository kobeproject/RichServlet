
package com.apache.rich.servlet.http.servlet.realize.configuration;

import javax.servlet.Filter;
import javax.servlet.ServletException;

import com.apache.rich.servlet.http.servlet.realize.adapter.RichServletHttpComponentConfigurationAdapter;

public class RichServletHttpFilterConfiguration extends RichServletHttpComponentConfigurationAdapter<Filter, RichServletHttpFilterConfig> {

    public RichServletHttpFilterConfiguration(Class<? extends Filter> servletClazz,
                               String... urlPatterns) {
        super(servletClazz, urlPatterns);
    }

    public RichServletHttpFilterConfiguration(Class<? extends Filter> componentClazz) {
        super(componentClazz);
    }

    public RichServletHttpFilterConfiguration(Filter component, String... urlPatterns) {
        super(component, urlPatterns);
    }

    public RichServletHttpFilterConfiguration(Filter servlet) {
        super(servlet);
    }

    @Override
    protected void doInit() throws ServletException {
        this.component.init(this.config);
    }

    @Override
    protected void doDestroy() throws ServletException {
        this.component.destroy();
    }

    @Override
    protected RichServletHttpFilterConfig newConfigInstance(
            Class<? extends Filter> componentClazz) {
        return new RichServletHttpFilterConfig(componentClazz.getName());
    }

    public RichServletHttpFilterConfiguration addInitParameter(String name, String value) {
        super.addConfigInitParameter(name, value);
        return this;
    }

}
