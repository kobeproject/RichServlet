
package com.apache.rich.servlet.http.servlet.realize.configuration;

import javax.servlet.Filter;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServlet;
import java.io.File;
import java.util.*;

/**
 * 上下文配置
 * @author wanghailing
 *
 */
public class RichServletHttpWebappConfiguration {

    private String name;

    private int sessionTimeout = 60 * 60; // 1 hour

    private Map<String, String> contextParameters;

    private Collection<RichServletHttpServletContextListenerConfiguration> contextListeners;

    private Collection<RichServletHttpFilterConfiguration> filters;

    private Collection<RichServletHttpServletConfiguration> servlets;

    private File staticResourcesFolder;

    public RichServletHttpWebappConfiguration addContextParameter(String name, String value) {

        if (this.contextParameters == null)
            this.contextParameters = new HashMap<String, String>();

        this.contextParameters.put(name, value);
        return this;
    }

    public RichServletHttpWebappConfiguration addServletContextListener(
            Class<? extends ServletContextListener> listenerClass) {
        return this
                .addServletContextListenerConfigurations(new RichServletHttpServletContextListenerConfiguration(
                        listenerClass));
    }

    public RichServletHttpWebappConfiguration addServletContextListener(
            ServletContextListener listener) {
        return this
                .addServletContextListenerConfigurations(new RichServletHttpServletContextListenerConfiguration(
                        listener));
    }

    public RichServletHttpWebappConfiguration addServletContextListenerConfigurations(
            RichServletHttpServletContextListenerConfiguration... configs) {

        if (configs == null || configs.length == 0)
            return this;

        return this.addServletContextListenerConfigurations(Arrays
                .asList(configs));
    }

    public RichServletHttpWebappConfiguration addServletContextListenerConfigurations(
            List<RichServletHttpServletContextListenerConfiguration> configs) {
        if (configs == null || configs.size() == 0)
            return this;

        if (this.contextListeners == null)
            this.contextListeners = new ArrayList<RichServletHttpServletContextListenerConfiguration>();

        this.contextListeners.addAll(configs);
        return this;
    }

    public Collection<RichServletHttpServletContextListenerConfiguration> getServletContextListenerConfigurations() {
        return this.contextListeners != null ? Collections
                .unmodifiableCollection(this.contextListeners) : null;
    }

    public Map<String, String> getContextParameters() {
        return this.contextParameters != null ? Collections
                .unmodifiableMap(this.contextParameters) : null;
    }

    public RichServletHttpWebappConfiguration setName(String name) {
        this.name = name;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public RichServletHttpWebappConfiguration setSessionTimeout(int sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
        return this;
    }

    public int getSessionTimeout() {
        return sessionTimeout;
    }

    public RichServletHttpWebappConfiguration addFilter(Filter filter) {
        return this.addFilterConfigurations(new RichServletHttpFilterConfiguration(filter));
    }

    public RichServletHttpWebappConfiguration addFilter(Filter filter, String... urlPatterns) {
        return this.addFilterConfigurations(new RichServletHttpFilterConfiguration(filter,
                urlPatterns));
    }

    public RichServletHttpWebappConfiguration addFilter(Class<? extends Filter> filterClass) {
        return this
                .addFilterConfigurations(new RichServletHttpFilterConfiguration(filterClass));
    }

    public RichServletHttpWebappConfiguration addFilter(Class<? extends Filter> filterClass,
                                         String... urlPatterns) {
        return this.addFilterConfigurations(new RichServletHttpFilterConfiguration(
                filterClass, urlPatterns));
    }

    public RichServletHttpWebappConfiguration addFilterConfigurations(
    		RichServletHttpFilterConfiguration... filters) {

        if (filters == null || filters.length == 0)
            return this;

        return this.addfilterConfigurations(Arrays.asList(filters));
    }

    public RichServletHttpWebappConfiguration addfilterConfigurations(
            Collection<RichServletHttpFilterConfiguration> configs) {

        if (configs == null || configs.size() == 0)
            return this;

        if (this.filters == null)
            this.filters = new ArrayList<RichServletHttpFilterConfiguration>();

        this.filters.addAll(configs);
        return this;
    }

    public Collection<RichServletHttpFilterConfiguration> getFilterConfigurations() {
        return this.filters != null ? Collections
                .unmodifiableCollection(this.filters) : null;
    }

    public boolean hasFilterConfigurations() {
        return this.filters != null && !this.filters.isEmpty();
    }

    public RichServletHttpWebappConfiguration addHttpServlet(HttpServlet servlet) {
        return this.addServletConfigurations(new RichServletHttpServletConfiguration(servlet));
    }

    public RichServletHttpWebappConfiguration addHttpServlet(HttpServlet servlet,
                                              String... urlPatterns) {
        return this.addServletConfigurations(new RichServletHttpServletConfiguration(servlet,
                urlPatterns));
    }

    public RichServletHttpWebappConfiguration addHttpServlet(
            Class<? extends HttpServlet> servletClass) {
        return this.addServletConfigurations(new RichServletHttpServletConfiguration(
                servletClass));
    }

    public RichServletHttpWebappConfiguration addHttpServlet(
            Class<? extends HttpServlet> servletClass, String... urlPatterns) {
        return this.addServletConfigurations(new RichServletHttpServletConfiguration(
                servletClass, urlPatterns));
    }

    public RichServletHttpWebappConfiguration addServletConfigurations(
    		RichServletHttpServletConfiguration... servlets) {

        if (servlets == null || servlets.length == 0)
            return this;

        return this.addServletConfigurations(Arrays.asList(servlets));
    }

    public RichServletHttpWebappConfiguration addServletConfigurations(
            Collection<RichServletHttpServletConfiguration> configs) {

        if (configs == null || configs.size() == 0)
            return this;

        if (this.servlets == null)
            this.servlets = new ArrayList<RichServletHttpServletConfiguration>();

        this.servlets.addAll(configs);
        return this;
    }

    public Collection<RichServletHttpServletConfiguration> getServletConfigurations() {
        return this.servlets != null ? Collections
                .unmodifiableCollection(this.servlets) : null;
    }

    public boolean hasServletConfigurations() {
        return this.servlets != null && !this.servlets.isEmpty();
    }

    public RichServletHttpWebappConfiguration setStaticResourcesFolder(String folder) {
        return this.setStaticResourcesFolder(new File(folder));
    }

    public RichServletHttpWebappConfiguration setStaticResourcesFolder(File folder) {
        if (folder == null)
            throw new IllegalArgumentException(
                    "Static resources folder must be not null!");

        if (!folder.exists())
            throw new IllegalArgumentException("Static resources folder '"
                    + folder.getAbsolutePath() + "' was not found!");

        if (!folder.isDirectory())
            throw new IllegalArgumentException("Static resources folder '"
                    + folder.getAbsolutePath() + "' must be a directory!");

        this.staticResourcesFolder = folder;
        return this;
    }

    public File getStaticResourcesFolder() {
        return staticResourcesFolder;
    }
}
