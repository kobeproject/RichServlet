/**
 * 
 */
package com.apache.rich.servlet.server.enums;

/**
 * @author wanghailing
 *
 */
public enum RichServletServerEnums {
	
	http,http2,httpservlet;
}
