/**
 * 
 */
package com.apache.rich.servlet.server.classloader;


import com.apache.rich.servlet.server.xml.XmlParse;

import com.apache.rich.servlet.server.enums.FileTypeEnums;

/**
 * 
 * @author xuanjing.lb
 *
 */
public class RichServletClassloader extends ClassLoader {
	
	private CustomerJarLoader customerJarLoader;
	
	private String path;
	
	private XmlParse xmlParse;
	
	public RichServletClassloader(Class<?> cls){
		path=getRootPath(cls);
		customerJarLoader=initJarClass(cls);
		xmlParse=new XmlParse();
	}
	
	/**
	 * 初始化 jar包的class
	 */
	private CustomerJarLoader initJarClass(Class<?> cls){
		if(path.endsWith(FileTypeEnums.JAR.getCode())){//jar包
			return new CustomerJarLoader(path);
		}
		return null;
	}
	
	@Override
	public Class<?> findClass(String name) throws ClassNotFoundException {
		if(path.endsWith(FileTypeEnums.JAR.getCode())&&customerJarLoader!=null){//jar包
			return customerJarLoader.loadClass(name, true);
		}else{
			CustomerClassLoader customerClassLoader=new CustomerClassLoader(path, name);
			return customerClassLoader.loadClass(name, true);
		}
	}
	
	/**
	 * 获取
	 * @return
	 */
	private String getRootPath(Class<?> cls){
		String path=cls.getProtectionDomain().getCodeSource().getLocation().getPath();
		return path;
	}

	/**
	 * @return the xmlParse
	 */
	public XmlParse getXmlParse() {
		return xmlParse;
	}
	
	
}
