/**
 * 
 */
package com.apache.rich.servlet.http.server.acceptor;

import java.util.concurrent.TimeUnit;

import io.netty.channel.socket.SocketChannel;
import com.apache.rich.servlet.http.server.handler.RichServletAsyncHttpRequestHandler;
import com.apache.rich.servlet.core.server.helper.RichServletServerOptions;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.timeout.ReadTimeoutHandler;
import com.apache.rich.servlet.core.server.RichServletServer;
import com.apache.rich.servlet.core.server.helper.RichServletServerOptionProvider;
import io.netty.channel.ChannelPipeline;
import com.apache.rich.servlet.core.server.acceptor.AsyncAcceptor;

/**
 * @author wanghailing
 *
 */
public class RichServletHttpServerAsyncAcceptor extends AsyncAcceptor {

	public RichServletHttpServerAsyncAcceptor(
			RichServletServer richServletServer) {
		super(richServletServer);
	}

	/* (non-Javadoc)
	 * @see com.apache.rich.servlet.core.server.acceptor.AsyncAcceptor#protocolPipeline(io.netty.channel.ChannelPipeline, com.apache.rich.servlet.core.server.helper.RichServletServerOptionProvider)
	 */
	@Override
	protected void protocolPipeline(SocketChannel ch,
			RichServletServerOptionProvider options) throws Exception{
		ch.pipeline().addLast("richservlet-http-timer", new ReadTimeoutHandler(options.option(RichServletServerOptions.TCP_TIMEOUT), TimeUnit.MILLISECONDS))
        		.addLast("richservlet-http-decoder", new HttpRequestDecoder())
        		.addLast("richservlet-http-aggregator", new HttpObjectAggregator(options.option(RichServletServerOptions.MAX_PACKET_SIZE)))
        		.addLast("richservlet-http-request-poster", RichServletAsyncHttpRequestHandler.build())
        		.addLast("richservlet-http-encoder", new HttpResponseEncoder());
	}

}
