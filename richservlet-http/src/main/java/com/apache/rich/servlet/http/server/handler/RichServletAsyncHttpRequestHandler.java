package com.apache.rich.servlet.http.server.handler;


import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpUtil;

import static io.netty.handler.codec.http.HttpResponseStatus.CONTINUE;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

import com.apache.rich.servlet.core.server.rest.response.RichServletServerHttpResponseBuilder;
import com.apache.rich.servlet.core.server.rest.response.RichServletServerHttpResponse;
import com.apache.rich.servlet.core.server.acceptor.AsyncRequestHandler;
import com.apache.rich.servlet.core.server.rest.request.NettyHttpRequest;
import com.apache.rich.servlet.core.server.rest.ExecutorTask;
import com.apache.rich.servlet.core.server.rest.HttpContext;
import com.apache.rich.servlet.core.server.rest.HttpURLResource;
import com.apache.rich.servlet.core.server.monitor.RichServletServerMonitor;
import com.apache.rich.servlet.core.server.rest.controller.URLController;
import com.apache.rich.servlet.common.enums.RequestMethodEnums;
import io.netty.handler.codec.http.DefaultHttpResponse;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.timeout.ReadTimeoutException;

/**
 * 
 * @author wanghailing
 *
 */
public class RichServletAsyncHttpRequestHandler extends AsyncRequestHandler {

    // Http context
    private volatile HttpContext httpContext;
    // channel context
    private ChannelHandlerContext context;

    public static RichServletAsyncHttpRequestHandler build() {
        return new RichServletAsyncHttpRequestHandler();
    }

    @Override
    protected void channelRead0(final ChannelHandlerContext ctx, final FullHttpRequest httpRequest) throws Exception {
    		if (HttpUtil.is100ContinueExpected(httpRequest)) {
    			ctx.writeAndFlush(new DefaultFullHttpResponse(HTTP_1_1, CONTINUE));
    	    }
    		this.context = ctx;

        // build context
        httpContext = HttpContext.build(new NettyHttpRequest(context.channel(), httpRequest));
        // execute async logic code block
        doRun();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if (cause instanceof ReadTimeoutException) {
            // httpcode 504
            if (context.channel().isOpen()){
    				RichServletServerHttpResponse hServerHttpResponse=new RichServletServerHttpResponse();
    				hServerHttpResponse.setHttpResponse(RichServletServerHttpResponseBuilder.create(httpContext, HttpResponseStatus.GATEWAY_TIMEOUT));
            		sendResponse(httpContext, hServerHttpResponse);
            }
        }
    }

    private void doRun() {

        /**
         * 1. checking phase. http method, param, url
         *
         */
        if (!checkup())
            return;

        /**
         * 2. according to URL to search the URLController
         *
         */
        URLController controller = findController();

        /**
         * 3. execute controller logic to async executor thread pool
         *
         */
        if (controller != null)
            execute(controller);
    }

    private boolean checkup() {
        // check http method
        if (httpContext.getRequestMethod() == RequestMethodEnums.UNKOWN) {
            // httpcode 405
			RichServletServerHttpResponse hServerHttpResponse=new RichServletServerHttpResponse();
			hServerHttpResponse.setHttpResponse(RichServletServerHttpResponseBuilder.create(httpContext, HttpResponseStatus.METHOD_NOT_ALLOWED));
            sendResponse(httpContext, hServerHttpResponse);
            return false;
        }

        return true;
    }

    private URLController findController() {
        // build URLResource from incoming http request
        HttpURLResource resource = HttpURLResource.fromHttp(httpContext.getUri(), httpContext.getRequestMethod());
        URLController controller;
        if ((controller = controllerRouter.findURLController(resource)) == null) {
            // httpcode 404
        	 	
            
    			RichServletServerHttpResponse hServerHttpResponse=new RichServletServerHttpResponse();
    			hServerHttpResponse.setHttpResponse(RichServletServerHttpResponseBuilder.create(httpContext, HttpResponseStatus.NOT_FOUND));
            sendResponse(httpContext,hServerHttpResponse );
            RichServletServerMonitor.incrRequestMiss();
            return null;
        }

        if (!controller.isInternal()) {
            RichServletServerMonitor.incrRequestHit();
            controller.hit();
        }

        return controller;
    }

    private void execute(URLController controller) {
        if (!controller.isInternal()) {
            RichServletServerMonitor.setLastServTime(System.currentTimeMillis());
            RichServletServerMonitor.setLastServID(httpContext.getRequestId());
        }

        // if IOWorker pool is empty. we use this same thread directly.
        // no more context switch (queue pop/push cause) happen here !
        if (ioWorker == null) {
            try {
                sendResponse(httpContext, new ExecutorTask(httpContext, interceptor, controller).call());
            } catch (Throwable e) {
                e.printStackTrace();
                sendResponse(httpContext, failure());
            }
            return;
        }

        executeAsyncTask(new ExecutorTask(httpContext, interceptor, controller));
    }

    private void executeAsyncTask(ExecutorTask task) {
        final HttpContext currentContext = httpContext;

        Futures.addCallback(ioWorker.submit(task), new FutureCallback<RichServletServerHttpResponse>() {
            @Override
            public void onSuccess(RichServletServerHttpResponse resp) {
                sendResponse(currentContext, resp);
            }

            @Override
            public void onFailure(Throwable e) {
                e.printStackTrace();
                sendResponse(currentContext, failure());
            }
        });
    }

    private RichServletServerHttpResponse failure() {
    		RichServletServerHttpResponse hServerHttpResponse=new RichServletServerHttpResponse();
        RichServletServerMonitor.setLastServFailID(httpContext.getRequestId());
        // httpcode 503
        DefaultHttpResponse httpResponse= RichServletServerHttpResponseBuilder.create(httpContext, HttpResponseStatus.SERVICE_UNAVAILABLE);
        hServerHttpResponse.setHttpResponse(httpResponse);
        return hServerHttpResponse;
    }

    private void sendResponse(HttpContext ctx, RichServletServerHttpResponse response) {

    		if(context.channel().isOpen()){
    			ChannelFuture future = context.channel().writeAndFlush(response.getHttpResponse());
    	        // http short connection
    	        if (!ctx.isKeepAlive()||response.getHttpResponse().getStatus().equals(HttpResponseStatus.OK)){
    	            future.addListener(ChannelFutureListener.CLOSE);
    	        }
    		}
    		
    }
}
