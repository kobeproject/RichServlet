package test.apache.rich.servlet.http.server.rest;


import java.util.List;

import com.alibaba.fastjson.JSONObject;

import com.apache.rich.servlet.core.annotation.RequestBody;
import com.apache.rich.servlet.common.enums.RequestMethodEnums;
import com.apache.rich.servlet.core.annotation.Controller;
import com.apache.rich.servlet.core.annotation.RequestParam;
import com.apache.rich.servlet.core.annotation.RequestMapping;

@RequestMapping("/web/api")
@Controller
public class Controller3 {

    @RequestMapping(value = "/openTabs.json", method = RequestMethodEnums.GET)
    public JSONObject getTabRecord(@RequestParam(value = "projectId", required = false) Long projectId,
                                        @RequestBody List<Integer> tabContent) {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("projectId", projectId);
        jsonObject.put("tabContent", tabContent);
        return jsonObject;
    }

}